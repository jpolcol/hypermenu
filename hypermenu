#!/bin/sh

# *-------------------------------------------------------------------------*
# HyperMenu

# Copyright 2014, James Polanco <jpolcol@gmail.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Usage: hypermenu [todo] [todo category] [note] [calc] [apps] [run] [gtk2theme]
#                  [system] [weather]
# *-------------------------------------------------------------------------*

#***********#
# FUNCTIONS #
#***********#

###########
# UTILITY #
###########

# Generate the separator based on the width dmenu title
# The required argument is a string
# The optional second arguement is a number which divides the length of the separator
GenerateSeparator () {
    if [ ! -z $2 ]; then
        # Used for spliting the length of the separator
        SEPARATOR_LENGTH=$(((SEPARATOR_TOTAL_CHARS - (${#1} + 2))/$2)) 
    else
        # 2 represents the padding around the dmenu title
        SEPARATOR_LENGTH=$((SEPARATOR_TOTAL_CHARS - (${#1} + 2)))
    fi

    SEPARATOR=$(printf -v f "%"$SEPARATOR_LENGTH"s" ; printf "%s" "${f// /$SEPARATOR_CHAR}")
}

CheckCommand () {
    command -v $1 >/dev/null 2>&1 || \
        { 
            Error "^fg(black)^bg(red) ERROR ^fg(red)^bg(black) $1 is required."
            exit 1
        }
}

############
# SETTINGS #
############

SettingsApply () {
    . "$SETTINGS_FILE"
}

SettingsInit () {
    # Blank settings file
    :> "$SETTINGS_FILE"

    echo "MENU_FONT=fixed:size=8.5" >> "$SETTINGS_FILE"
    echo "RANDOM_COLOR=true" >> "$SETTINGS_FILE"
    echo "COLOR=#FFFFFF" >> "$SETTINGS_FILE"
    echo "TERMINAL_EDITOR=true" >> "$SETTINGS_FILE"
    echo "SEPARATOR_CHAR=\"·\"" >> "$SETTINGS_FILE"
    echo "SEPARATOR_TOTAL_CHARS=169" >> "$SETTINGS_FILE"
    echo "MAX_LINES=53" >> "$SETTINGS_FILE"
    echo "CALC_MAX_HISTORY=20" >> "$SETTINGS_FILE"
    echo "CALC_SHOW_OPERATORS=true" >> "$SETTINGS_FILE"
    echo "CALC_SHOW_HISTORY=true" >> "$SETTINGS_FILE"
    echo "TODO_CATEGORY_ORDER=first" >> "$SETTINGS_FILE"
    echo "RECORD_COMPLETED_TODOS=true" >> "$SETTINGS_FILE"
    echo "RECORD_COMPLETED_NOTES=true" >> "$SETTINGS_FILE"
    echo "EXIT_ON_ERRORS=false" >> "$SETTINGS_FILE"
    echo "ERROR_MESSAGE_DURATION=3" >> "$SETTINGS_FILE"
    echo "SYSTEM_WWW_IP_SHOW=false" >> "$SETTINGS_FILE"
    echo "WEATHER_METRIC=F" >> "$SETTINGS_FILE"
    echo "WEATHER_URL=http://www.accuweather.com/en/us/dallas-tx/75238/current-weather/32615_pc" >> "$SETTINGS_FILE"
}

SettingsCheck () {
    # If the settings file doesn't exist
    if [ ! -e "$SETTINGS_FILE" ] || [ -z "$(<"$SETTINGS_FILE")" ]; then
        # Generate default settings
        SettingsInit
    fi
}

# Takes parameters: setting, value, quote
# Quote: places escaped quotes before the value
Set () {
    args=$(getopt -o s:v:q --long setting:,value:,quote -- "$@")

    eval set -- "$args"

    while [ ! -z "$1" ]; do
        case "$1" in
        -s|--setting) SETTING="$2" ;;
        -v|--value) VALUE="$2"  ;;
        -q|--quote) QUOTE=true ;;
        esac

        shift
    done

    SETTINGS=$(awk -v SETTING="$SETTING" -v VALUE="$VALUE" -v QUOTE=$QUOTE -F= \
        '{
            if ($1 == SETTING) {
                if (QUOTE)
                    $2 = "\""VALUE"\""
                else
                    $2 = VALUE
            }

            print $1"="$2
        '} "$SETTINGS_FILE")

    # Make the setting/variable assignment active right now
    eval $SETTING="\$VALUE" 

    echo -e "$SETTINGS" > "$SETTINGS_FILE"
}

##############
# INITIALIZE #
##############

HyperMenuInit () {
    HYPERMENU="$HOME"/.hypermenu

    if [ ! -e "$HYPERMENU" ]; then
        mkdir "$HYPERMENU"
    fi

    SETTINGS_FILE="$HYPERMENU"/settings

    SettingsCheck
    SettingsApply

    CALC_HISTORY_FILE="$HYPERMENU"/calc_history

    if [ ! -e "$CALC_HISTORY_FILE" ]; then
        touch "$CALC_HISTORY_FILE"
    fi

    TODOS_FILE="$HYPERMENU"/todos

    if [ ! -e "$TODOS_FILE" ]; then
        echo -e "DOG 1: Give it a try!\nDOG 1: Category sorting is invoked by executing hypermenu as \"hypermenu todo category\"\nCAT1: The colon can even come after multiple words, like the entry below\nCAT1: Notice the colon \":\" ... it designates a category\nCAT1: This entry is categorized as CAT1\nThis TODO entry has NO category" > "$TODOS_FILE"
    fi

    DONE_TODOS_FILE="$HYPERMENU"/done_todos

    if [ ! -e "$DONE_TODOS_FILE" ]; then
        touch "$DONE_TODOS_FILE"
    fi

    NOTES_FILE="$HYPERMENU"/notes

    if [ ! -e "$NOTES_FILE" ]; then
        touch "$NOTES_FILE"
    fi

    DONE_NOTES_FILE="$HYPERMENU"/done_notes

    if [ ! -e "$DONE_NOTES_FILE" ]; then
        touch "$DONE_NOTES_FILE"
    fi

    CONFIGS_FILE="$HYPERMENU"/configs

    if [ ! -e "$CONFIGS_FILE" ]; then
        echo -e ".Xresources;"$HOME"/.Xresources\n.xinitrc;"$HOME"/.xinitrc\n.vimrc;"$HOME"/.vimrc" > "$CONFIGS_FILE"
    fi

    PROGRAMS_FILE="$HYPERMENU"/programs

    if [ ! -e "$PROGRAMS_FILE" ]; then
        echo -e "web browser;firefox\neditor;gvim\nterminal;xterm\nfile manager;thunar" > "$PROGRAMS_FILE"
    fi

    WEATHER_URLS_FILE="$HYPERMENU"/weather_urls

    if [ ! -e "$WEATHER_URLS_FILE" ]; then
        touch "$WEATHER_URLS_FILE"
    fi

    # Light colors
    COLORS=("#85BB65" "#FF0038" "#CC4E5C" "#C19A6B" "#D39BCB" "#F8DE7E" "#96C8A2" "#FF2800"
            "#6699CC" "#FFFF00" "#B19CD9" "#F0E68C" "#3F00FF" "#6D9BC3" "#1F75FE" "#E0218A" 
            "#E5B73B" "#73A9C2" "#1E90FF" "#74C365" "#9EFD38" "#F4CA16" "#EB4C42" "#C8A2C8"
            "#FF4F00" "#FE6F5E" "#ADDFAD" "#00FFFF" "#73C2FB" "#FFA07A" "#BA55D3" "#9F8170"
            "#C9DC87" "#0070FF" "#00FF7F" "#FF8C00" "#80DAEB" "#D473D4" "#B2EC5D" "#778899"
            "#FFD300" "#C2B280" "#997A8D" "#66DDAA" "#7FFF00" "#8C92AC" "#DE6FA1" "#007FFF"
            "#EF3038" "#6495ED" "#FB607F" "#DEB887" "#E3F988" "#FF7E00" "#CD7F32" "#A2A2D0"
            "#ED872D" "#C74375" "#FF7F50" "#FF004F" "#7DF9FF" "#808080" "#FF033E" "#DE3163"
            "#C54B8C" "#F64A8A" "#E97451" "#BF4F51" "#CCFF00" "#7CB9E8" "#6050DC" "#F19CBB"
            "#F88379" "#FF55A3" "#FF9933" "#4F86F7" "#6CA0DC" "#A3C1AD" "#F0E130" "#FF8243"
            "#FFF700" "#979AAA" "#A9A9A9" "#AD6F69" "#FF0800" "#4997D0" "#72A0C1" "#E1A95F"
            "#00BFFF" "#BEBEBE" "#87A96B" "#44D7A8" "#B2BEB5" "#FFC40C" "#00FFFF" "#E3FF00"                                                      
            "#EA3C53" "#93CCEA" "#B284BE" "#A8E4A0" "#BA8759" "#9955BB" "#FFE135" "#8AB9F1"                                                      
            "#F0E68C" "#318CE7" "#C19A6B" "#B94E48" "#F88379" "#DA8A67" "#8CBED6" "#E9D66B"                                                      
            "#7FFFD4" "#7BB661" "#FF3800" "#DF73FF" "#446CCF" "#FF1493" "#6699CC" "#D4AF37"                                                      
            "#00BFFF" "#6F00FF" "#F8B878" "#FFDF00" "#A95C68" "#C95A49" "#FF55A3" "#00FF00"                                                      
            "#CB4154" "#C154C1" "#B57EDC" "#B38B6D" "#9370DB" "#E97451" "#BDDA57" "#6082B6"                                                      
            "#5218FA" "#D473D4" "#A57164" "#EC3B83" "#FFA700" "#E25822" "#FF69B4" "#FF4040"
            "#F984EF" "#F56991" "#FF1493" "#CB6D51" "#91A3B0" "#FFEF00" "#50C878" "#C19A6B"                                                      
            "#FFDB58" "#A9BA9D" "#E66771" "#DFFF00" "#CCFF00" "#1C1CF0" "#DE3163" "#9932CC"                                                      
            "#FCF75E" "#FDFF00" "#8F00FF" "#8F9779" "#D1BEA8" "#FFF44F" "#666699" "#DE5D83"                                                      
            "#CD5B45" "#E9692C" "#8FBC8F" "#87CEFA" "#966FD6" "#DDA0DD" "#BF00FF" "#FBEC5D"                                                      
            "#9457EB" "#71A6D2" "#996666" "#E9967A" "#FFFF31" "#967BB6" "#FF00FF" "#8C92AC"                                                      
            "#FDEE00" "#32CD32" "#BFFF00" "#D73B3E" "#F5C71A" "#DE5285" "#92A1CF" "#E4717A"
            "#B48395" "#E9D66B" "#98777B" "#FF9966" "#99BADD" "#CD9575" "#E68FAC" "#A67B5B"
            "#00FF00" "#EE82EE" "#FBEC5D" "#F08080" "#BF94E4" "#CD5C5C" "#C154C1" "#FFBF00"                                                      
            "#6F00FF" "#C3B091" "#A4C639" "#FE5A1D" "#EEDC82" "#00FF00" "#77B5FE" "#3FFF00"                                                      
            "#E32636" "#E5AA70" "#DA614E" "#FFFF66" "#9966CC" "#A99A86" "#D9603B" "#5F9EA0"                                                       
            "#BDB76B" "#D0417E" "#ED2939" "#48D1CC" "#5A4FCF" "#808080" "#8A2BE2" "#FF003F"                                                       
            "#CC6666" "#FF00FF" "#7B68EE" "#E75480" "#FF77FF" "#0247FE" "#98817B" "#FF1DCE"                                                       
            "#E3A857" "#FFBF00" "#E62020" "#DA3287" "#E68FAC" "#E52B50" "#90EE90" "#0000FF"                                                       
            "#C19A6B" "#ADFF2F" "#E03C31" "#FFD700" "#FDD5B1" "#FF0090" "#848482" "#ED9121")                                                      

    if $RANDOM_COLOR; then
        COLOR=${COLORS[$RANDOM % ${#COLORS[@]}]}

        # DEBUG: identify a problematic color and remove from above color list
        #echo $COLOR > /tmp/color
    fi

    DMENU='dmenu -l '$MAX_LINES' -i -b -fn '$MENU_FONT' -nb #000000 -nf '$COLOR' -sf #000000 -sb '$COLOR
    DMENU_HOR='dmenu -i -b -fn '$MENU_FONT' -nb #000000 -nf '$COLOR' -sf #000000 -sb '$COLOR
    DMENU_RUN='dmenu_run -i -b -fn '$MENU_FONT' -nb #000000 -nf '$COLOR' -sf #000000 -sb '$COLOR
}

#########
# ERROR #
#########

Error () {
    # Get X screen resolution
    X_SCREEN_RESOLUTION=$(xdpyinfo | awk \
        '{ 
            if ($1 == "dimensions:") { 
                sub("x", " ", $2)
                print $2
            } 
        }')

    WINDOW_WIDTH=$(awk '{ print $1 }' <<< "$X_SCREEN_RESOLUTION")
    WINDOW_HEIGHT=$(awk '{ print $2 }' <<< "$X_SCREEN_RESOLUTION")

    echo "$1" | dzen2 -w $WINDOW_WIDTH -x 0 -y $WINDOW_HEIGHT \
        -p $ERROR_MESSAGE_DURATION -fn $MENU_FONT -ta l -bg #000000 -fg #FF0000
}

#######
# RUN #
#######

Run () {
    $DMENU_RUN
}

########
# TODO #
########

Todo () {
    if [ "$1" == "category" ]; then
        CATEGORIES=$(cut -d: -f1 "$TODOS_FILE" | sort | uniq -d)

        GREPS=$(echo -e "$CATEGORIES" | awk \
            '{ 
                if (NR == 1) 
                    printf("^%s:", $0)
                else
                    printf("\\|^%s:", $0)
            }')

        # Format categories with brackets
        CATEGORIES=$(echo -e "$CATEGORIES" | awk '{ print "["$0"]" }')

        CONTINUE=false

        while [ 1 ]; do
            TODOS=$(tac "$TODOS_FILE" | grep -v "$GREPS")

            if [ "$TODO_CATEGORY_ORDER" == "first" ]; then
                TODO=$(echo -e "$CATEGORIES\n$TODOS" | $DMENU -p TODO)
            elif [ "$TODO_CATEGORY_ORDER" == "last" ]; then
                TODO=$(echo -e "$TODOS\n$CATEGORIES" | $DMENU -p TODO)
            else
                Error "^fg(#000000)^bg(#FF0000) ERROR ^fg(#FF0000)^bg(#000000) TODO_CATEGORY_ORDER variable must be either \"first\" or \"last\""
                exit 1
            fi

            if [ -z "$TODO" ]; then
                exit 0
            fi

            while read -r CATEGORY; do
                if [ "$TODO" == "$CATEGORY" ]; then
                    CATEGORY=$(echo "$CATEGORY" | awk \
                        '{ print substr($0, 2, length($0) - 2) }')

                    TODOS=$(grep "^$CATEGORY:" "$TODOS_FILE" | \
                        sed 's/^.*: //' | tac)

                    TODO=$(echo -e "$TODOS" | $DMENU -p "$CATEGORY")

                    if [ -z "$TODO" ]; then
                        CONTINUE=true   
                    else
                        TODO="$CATEGORY: $TODO"
                    fi

                    break
                fi
            done <<< "$CATEGORIES"
            
            if $CONTINUE; then
                CONTINUE=false
                continue
            elif [ $(echo -e "$CATEGORIES" | grep -cx "$TODO") -eq 0 ]; then
                break
            fi
        done
    else
        TODO=$(tac "$TODOS_FILE" | $DMENU -p TODO)
    fi

    if [ -s "$TODOS_FILE" ]; then
        TODOS=$(awk -v TODO="$TODO" \
            '{ 
                if ($0 == TODO)
                    DO_NOT_ADD = 1
                else
                    print $0
            }

            END {
                if (DO_NOT_ADD != 1)
                    print TODO
                else
                    exit 1
            }' "$TODOS_FILE")

        if [ -z "$TODOS" ]; then
            # Clear out file to rid lingering line
            :> "$TODOS_FILE"  
        else
            # If the above awk statement exit status is 1
            # Then we are subtracting a todo
            if (( $? )); then
                if [ "$RECORD_COMPLETED_TODOS" == "true" ]; then
                    # Append todo to dones files
                    echo "$TODO" >> "$DONE_TODOS_FILE" 
                fi
            fi

            echo "$TODOS" > "$TODOS_FILE"
        fi
    else
        echo "$TODO" > "$TODOS_FILE"
    fi
}

########
# NOTE #
########

Note () {
    NOTE=$(tac "$NOTES_FILE" | $DMENU -p NOTE)

    if [ -s "$NOTES_FILE" ]; then
        NOTES=$(awk -v NOTE="$NOTE" \
            '{ 
                if ($0 == NOTE)
                    DO_NOT_ADD = 1
                else
                    print $0
            }

            END {
                if (DO_NOT_ADD != 1)
                    print NOTE
                else
                    exit 1
            }' "$NOTES_FILE")

        if [ -z "$NOTES" ]; then
            # Clear out file to rid lingering line
            :> "$NOTES_FILE"  
        else
            # If the above awk statement exit status is 1
            # Then we are subtracting a note
            if (( $? )); then
                if [ "$RECORD_COMPLETED_NOTES" == "true" ]; then
                    # Append note to done notes file
                    echo "$NOTE" >> "$DONE_NOTES_FILE" 
                fi
            fi

            echo "$NOTES" > "$NOTES_FILE"
        fi
    else
        echo "$NOTE" > "$NOTES_FILE"
    fi
}

##########
# SYSTEM #
##########

System () {
    GenerateSeparator "SYSTEM"

    MENU="$(< $PROGRAMS_FILE)\n\
$SEPARATOR\n\
[configs]\n\
$SEPARATOR\n\
sleep/then\n\
$SEPARATOR\n\
suspend;systemctl suspend\n\
hibernate;systemctl hibernate\n\
reboot;systemctl reboot\n\
poweroff;systemctl poweroff\n\
$SEPARATOR\n\
$(lscpu | awk -F: '{ if ($1 == "Core(s) per socket") { gsub(/^[ \t]+/, "", $2); cores = $2 } else if ($1 == "Model name") { gsub(/^[ \t]+/, "", $2); print $2"\ncore(s): "cores } }')\n\
$(free -m -h | awk '{ if ($1 == "Mem:") ram = $2; else if($1 == "-/+") print "RAM: "$3"/"ram; else if ($1 == "Swap:") print "swap: "$3"/"$2 }')\n\
$(df -h | awk '{ if (index($1, "/dev/")) { gsub(/\/dev\//, "", $1); print $1" @ "$6": "$3"/"$2" - "$5" used" } }')\n\
$SEPARATOR\n\
ARCH LINUX :: \
$(uname -r) :: \
$(lscpu | awk -F: '{ if ($1 == "Architecture") { gsub(/^[ \t]+/, "", $2); print $2 } }')\n\
$USER@$(hostname)"
    
    LOCAL_IP=$(ip -o addr show | awk '/ 192/ { gsub(/\/.*/, "", $4); print $4; exit }')
    
    if [ ! -z $LOCAL_IP ]; then
        MENU="$MENU :: $LOCAL_IP"
    fi

    # Lookup and append WWW IP address
    if $SYSTEM_WWW_IP_SHOW; then
        WWW_IP=$(dig +short myip.opendns.com @resolver1.opendns.com)

        if [ ! -z $WWW_IP ]; then
            MENU="$MENU :: $WWW_IP"
        else
            MENU="$MENU :: OFFLINE"
        fi
    fi

    SYS=$(echo -e "$MENU" | cut -d ';' -f1 | $DMENU -p SYSTEM)

    if [ -z "$SYS" ]; then
        exit 0
    fi

    case "$SYS" in
    suspend|hibernate|reboot|poweroff)
        ANS=$(echo -e "Yes\nNo" | $DMENU -p "$SYS")
        
        if [ -z "$ANS" ]; then
            exit 0
        fi

        if [ "$ANS" == "Yes" ]; then
            COMMAND=$(echo -e "$MENU" | sed -n 's/'"$SYS"';//p')

            $COMMAND
        else
            exit 0
        fi
    ;;
    "sleep/then")
        CheckCommand sleepthen

        GenerateSeparator "SLEEP"

        TIMES="15s\n30s\n60s\n$SEPARATOR\n1m\n5m\n15m\n30m\n45m\n$SEPARATOR\n1h\n2h\n3h\n4h\n5h\n6h\n12h\n$SEPARATOR\n1d\n2d\n3d"

        TIME=$(echo -e "$TIMES" | $DMENU -p SLEEP)

        if [ -z $TIME -o "$TIME" == "$SEPARATOR" ]; then
            exit 0
        fi

        THENS="suspend\nhibernate\nreboot\npoweroff"

        THEN=$(echo -e "$THENS" | $DMENU -p "THEN")

        if [ -z $THEN ]; then
            exit 0
        fi

        $XTERM -T "$THEN in $TIME" -e sleepthen $TIME $THEN
    ;;
    "[configs]")
        CONFIGS="$(< $CONFIGS_FILE)"

        CONFIG=$(echo -e "$CONFIGS" | cut -d ';' -f1 | $DMENU -p CONFIGS)
        
        if [ -z "$CONFIG" ]; then
            exit 0
        fi

        CONFIGS=$(echo -e "$CONFIGS" | grep -m 1 -F "$CONFIG")

        CONFIG=${CONFIGS#"$CONFIG";}

        if [ "$TERMINAL_EDITOR" == "true" ]; then
            $XTERM -e $EDITOR "$CONFIG"
        elif [ "$TERMINAL_EDITOR" == "false" ]; then
            $EDITOR "$CONFIG"
        else
            Error "^fg(#000000)^bg(#FF0000) ERROR ^fg(#FF0000)^bg(#000000) TERMINAL_EDITOR variable must be either \"true\" or \"false\", not \"""$TERMINAL_EDITOR""\""
        fi
    ;;
    $SEPARATOR)
        hypermenu system
    ;;
    *)
        $(echo -e "$MENU" | grep -m 1 -F "$SYS" | cut -d ';' -f2)
    ;;
    esac
}

#############
# GTK2THEME #
#############

Gtk2theme () {
    CheckCommand gtk-theme-switch2

    GenerateSeparator "GTK2 THEME"

    THEMES=$(find /usr/share/themes -name "gtkrc" | awk -F/ \
        '{
            if ($5 != "Default") 
                print $5 
        }' | sort)

    THEMES_IN_HOME=$(ls -1 "$HOME"/.themes)

    if [ ! -z "$THEMES_IN_HOME" ]; then
        THEMES=$THEMES_IN_HOME"\n$SEPARATOR\n"$THEMES
    fi

    THEME=$(echo -e "$THEMES" | $DMENU -p "GTK2 THEME")

    if [ ! -z "$THEME" -a "$THEME" != "$SEPARATOR" ]; then
        if [ -z "$(grep $THEME <<< $THEMES_IN_HOME)" ]; then
            gtk-theme-switch2 $gtk_themedir/$THEME
        else
            gtk-theme-switch2 $THEME
        fi
    fi
}

########
# CALC #
########

Calc () {
    CheckCommand calc

    GenerateSeparator "CALC"

    if $CALC_SHOW_OPERATORS; then
        OPERATORS="+\n-\n*\n/\n**\n++\n--\n//\n%\n^"
    fi

    while [ 1 ]; do
        if $CALC_SHOW_HISTORY; then
            HISTORY=$(tac "$CALC_HISTORY_FILE" | head -n $CALC_MAX_HISTORY) 

            if [ ! -z "$HISTORY" ]; then
                RESULT=$(echo -e "$HISTORY\n$SEPARATOR\nclear history" | $DMENU -p CALC)

                if [ "$RESULT" == "clear history" ]; then
                    :> "$CALC_HISTORY_FILE"
                    continue
                fi
            else
                RESULT=$(echo | $DMENU_HOR -p CALC)
            fi
        else
            RESULT=$(echo | $DMENU_HOR -p CALC)
        fi

        # Escape key pressed?
        if [ -z "$RESULT" ]; then                   
            exit 0
        elif [ "$RESULT" == $SEPARATOR ]; then
            continue
        # Is the entry just a number?
        elif [ $RESULT -eq $RESULT ]; then          
            # Okay, but do not save the number to history
            SAVE_TO_HISTORY=false                   
        else
            # Capture stdout and stderr
            RESULT=$(calc -p "$RESULT" 2>&1)        

            # Handle errors
            if (( $? )); then

                if $EXIT_ON_ERRORS; then
                    exit 0
                else
                    continue
                fi
            fi
        fi

        if $SAVE_TO_HISTORY; then
            echo $RESULT >> "$CALC_HISTORY_FILE"
        else
            SAVE_TO_HISTORY=true
        fi

        break
    done

    while [ 1 ]; do
        RESULT_OLD="$RESULT"

        if $CALC_SHOW_HISTORY; then
            HISTORY=$(tac "$CALC_HISTORY_FILE" | head -n $CALC_MAX_HISTORY)
        fi

        if $CALC_SHOW_HISTORY && $CALC_SHOW_OPERATORS; then
            if [ ! -z "$HISTORY" ]; then
                GenerateSeparator "$RESULT_OLD"

                RESULT=$(echo -e "$OPERATORS\n$SEPARATOR\n$HISTORY" | $DMENU -p "$RESULT")
            else
                RESULT=$(echo -e "$OPERATORS" | $DMENU -p "$RESULT")
            fi
        elif $CALC_SHOW_HISTORY && ! $CALC_SHOW_OPERATORS; then
            if [ ! -z "$HISTORY" ]; then
                RESULT=$(echo -e "$HISTORY" | $DMENU -p "$RESULT")
            else
                RESULT=$($DMENU_HOR -p "$RESULT")
            fi
        elif ! $CALC_SHOW_HISTORY && $CALC_SHOW_OPERATORS; then
            RESULT=$(echo -e "$OPERATORS" | $DMENU -p "$RESULT")
        else
            RESULT=$($DMENU_HOR -p "$RESULT")
        fi

        if [ -z "$RESULT" ]; then
            exit 0
        elif [ "$RESULT" == "$SEPARATOR" ]; then
            RESULT=$RESULT_OLD
            continue
        elif [ $RESULT -eq $RESULT_OLD ]; then
            SAVE_TO_HISTORY=false
        elif [ $RESULT -eq $RESULT ]; then
            SAVE_TO_HISTORY=true
        fi

        # If first character in string is a number
        # Then assume user is entering a totally new calculation... 
        # Otherwise, use old result value
        if [ $(echo "$RESULT" | grep --regexp \^[0-9]) ]; then
            RESULT=$(calc -p "$RESULT" 2>&1)
        elif [ "$RESULT" == "++" ]; then
            RESULT=$(calc -p "$RESULT_OLD+1" 2>&1)
        elif [ "$RESULT" == "--" ]; then
            RESULT=$(calc -p "$RESULT_OLD-1" 2>&1)
        else
            # Combine old result (number in the prompt) with the new result 
            # To form a mathmatical expression
            RESULT=$(calc -p "$RESULT_OLD$RESULT" 2>&1)
        fi

        # Handle errors
        RESULT=$(calc -p "$RESULT" 2>&1)

        if (( $? )); then
            Error "^fg(#000000)^bg(#FF0000) CALC ^fg(#FF0000)^bg(#000000) Error: $(echo -e "$RESULT" | head -n1)"

            if $EXIT_ON_ERRORS; then
                exit 0
            else
                RESULT=$RESULT_OLD
                SAVE_TO_HISTORY=true
                continue
            fi
        fi
        
        if $SAVE_TO_HISTORY; then
            echo $RESULT >> "$CALC_HISTORY_FILE"
        else
            SAVE_TO_HISTORY=true
        fi
    done
}

########
# APPS #
########

Apps () {
    CheckCommand xdg_menu

    MENU=$(xdg_menu --format=twm)
        
    while [ 1 ]; do
        CATEGORY=$(echo -e "$MENU" | awk -F'"' \
            '{ 
                if ($1 == "}") 
                    exit
                
                if (x >= 2) 
                    print $2
                    x += 1
            }' | $DMENU -p "APPS")

        if [ -z "$CATEGORY" ]; then
            exit 0 
        fi

        PROGRAMS=$(echo -e "$MENU" | awk -F'"' -v category="$CATEGORY" \
            '{
                if ($1 == "menu " && $2 == category) {
                    go = 1
                    next
                }

                if (go) {
                    if ($1 == "{")
                        next

                    if ($1 == " }")
                        exit

                    print $0
                }
            }')

        PROGRAM=$(echo -e "$PROGRAMS" | awk -F'"' \
            '{ print $2 }' | $DMENU -p "$CATEGORY")

        if [ -z "$PROGRAM" ]; then
            continue
        fi

        EXEC=$(echo -e "$PROGRAMS" | awk -F'"' -v program="$PROGRAM" \
            '{ 
                if ($2 == program) {
                    sub(/&/, "", $4)
                    print $4
                }
            }')

        $EXEC &
        
        break
    done
}

###########
# WEATHER #
###########

Weather () {
    while [ 1 ]; do
        LOCATION=$(awk -F/ \
            '{
                if ($8 != "current-weather")
                    exit 1                    # URL must have current-weather in this field

                gsub(/-/, " ", $6)
                print toupper($6", "$7", "$5)
            }' <<< "$WEATHER_URL")

        # If "current-weather" not detected in URL (awk exit status), make fail
        if (( $? )); then
            Error "^fg(black)^bg(red) ERROR ^fg(red)^bg(black) WEATHER_URL does not contain \"current-weather\". Check URL in settings file."
            exit 1
        fi

        GenerateSeparator "WEATHER"

        WEATHER=$(curl -0 -s "$WEATHER_URL" | egrep -i \
            'Early AM|Today|Tonight|Overnight|"cond"|"temp"|Mon|Tue|Wed|Thu|Fri|Sat|Sun|country: ')

        WEATHER_METRIC_STANDARD=$(awk -F"'" \
            '{ 
                if (match($2, /US/) == 1) # if there are other countries that use F, add to this regex pattern
                    print "F"
                else
                    print "C"

                exit 0
            }' <<< "$WEATHER")

        CONVERT_TO_F="no"
        CONVERT_TO_C="no"

        if [ "$WEATHER_METRIC" != "$WEATHER_METRIC_STANDARD" ]; then
            if [ "$WEATHER_METRIC" == "F" ]; then
                CONVERT_TO_F="yes"
            elif [ "$WEATHER_METRIC" == "C" ]; then
                CONVERT_TO_C="yes"
            fi
        fi

        WEATHER=$(awk -F">" \
            -v MET="$WEATHER_METRIC" \
            -v SEP="$SEPARATOR" \
            -v LOC="$LOCATION" \
            -v CON_TO_F="$CONVERT_TO_F" \
            -v CON_TO_C="$CONVERT_TO_C" \
            '{ 
                gsub(/^[ \t]+/, "", $0)

                if ($2 == "<a href=\"#\"") { 
                    gsub(/<\/a/, "", $3)    # remove leading white space

                    switch ($3) {
                    case "Mon":
                        $3 = "MONDAY"
                        break
                    case "Tue":
                        $3 = "TUESDAY"
                        break
                    case "Wed":
                        $3 = "WEDNESDAY"
                        break
                    case "Thu":
                        $3 = "THURSDAY"
                        break
                    case "Fri":
                        $3 = "FRIDAY"
                        break
                    case "Sat":
                        $3 = "SATURDAY"
                        break
                    case "Sun":
                        $3 = "SUNDAY"
                        break
                    default:
                        $3 = toupper($3)
                        break
                    }

                    forecast=forecast"\n"SEP"\n"$3

                    next
                } 
                
                if ($1 == "<span class=\"cond\"") { 
                    gsub(/<\/span/, "", $2)
                    forecast=forecast"\n> "$2

                    next
                }
                
                if ($1 == "<strong class=\"temp\"") { 
                    gsub(/<span/, "", $2)
                    gsub(/<span/, "", $8)

                    if (CON_TO_C == "yes") {
                        $2 = ($2 - 32) / 1.8
                        $8 = ($8 - 32) / 1.8
                    } 
                    else if (CON_TO_F == "yes") {
                        $2 = ($2 * 1.8) + 32
                        $8 = ($8 * 1.8) + 32 
                    }

                    forecast=forecast"\n> High "int($2)"° "MET" :: Low "int($8)"° "MET

                    next
                } 

                if ($1 == "<span class=\"low\" style=\"color:#ffffff;\"") {
                    gsub(/<span/, "", $4)

                    if (CON_TO_C == "yes") {
                        $4 = ($4 - 32) / 1.8
                    } 
                    else if (CON_TO_F == "yes") {
                        $4 = ($4 * 1.8) + 32
                    }

                    forecast=forecast"\n> Low "int($4)"° "MET

                    next
                } 

                if ($2 == " <span class=\"cond\"") { 
                    gsub(/<\/span/, "", $3)
                    gsub(/<span/, "", $5)
                    split($9, realfeel, " ")
                    gsub(/<span/, "", realfeel[2])

                    if (CON_TO_C == "yes") {
                        $5 = ($5 - 32) / 1.8
                        realfeel[2] = (realfeel[2] - 32) / 1.8
                    } 
                    else if (CON_TO_F == "yes") {
                        $5 = ($5 * 1.8) + 32
                        realfeel[2] = (realfeel[2] * 1.8) + 32
                    }

                    current="CURRENTLY\n> "$3"\n> "int($5)"° "MET" [RealFeel® "int(realfeel[2])"° "MET"]"
                } 
            } END { 
                if (current == "" && forecast == "")
                    exit 1

                print LOC"\n"SEP"\n"current forecast"\n"SEP"\nChange Location\nChange Metric"
            }' <<< "$WEATHER")


        # If the above awk statement exit status is 1
        # Then there is an internet connectivity issue 
        if (( $? )); then
            Error "^fg(black)^bg(red) ERROR ^fg(red)^bg(black) Could not get weather. Accuweather server is either down or too slow, or your internet is disconnected or too slow."

            exit 1
        fi

        REPLY=$(echo "$WEATHER" | $DMENU -p "WEATHER")

        if [ -z "$REPLY" ]; then
            exit 0
        elif [ "$REPLY" == "Change Metric" ]; then
            METRIC=$(echo -e "F\nC" | $DMENU -p "METRIC")

            if [ -z "$METRIC" ]; then
                continue
            elif [ "$METRIC" == "F" ] || [ "$METRIC" == "C" ]; then
                Set -s "WEATHER_METRIC" -v "$METRIC"
            else
                Error "^fg(black)^bg(red) ERROR ^fg(red)^bg(black) \""$METRIC"\" is not a valid option."

                continue
            fi
        elif [ "$REPLY" == "Change Location" ]; then

            if [ -z "$(< $WEATHER_URLS_FILE)" ]; then
                echo "$WEATHER_URL" > "$WEATHER_URLS_FILE"
            fi

            LOCATIONS=$(awk -F/ \
                '{
                    URL = $0
                    gsub(/-/, " ", $6)
                    print toupper($6", "$7", "$5)";;"URL
                }' "$WEATHER_URLS_FILE")

            LOCATION=$(awk -F";;" '{ print $1 }' <<< "$LOCATIONS" | \
                $DMENU -p "LOCATIONS")

            if [ -z "$LOCATION" ]; then
                continue
            fi

            WEATHER_URL=$(awk -F";;" -v LOC="$LOCATION" \
                '{ 
                    if (index($1, LOC)) 
                        print $2 
                }' <<< "$LOCATIONS" )

            Set -s WEATHER_URL -v "$WEATHER_URL"
        else
            exit 0
        fi
    done
}

#*******#
# START #
#*******#

HyperMenuInit

if [ ! -z "$1" ]; then
    case $1 in
        'calc') Calc ;;
        'gtk2theme') Gtk2theme ;; 
        'note') Note ;; 
        'run') Run ;;
        'system') System ;;
        'todo') Todo "$2" ;;
        'apps') Apps ;;
        'weather') Weather ;;
        *) 
            Error "^fg(black)^bg(red) ERROR ^fg(red)^bg(black) \""$1"\" is not a valid option."
            exit 1
        ;;
    esac
else
    # If no option is specified
    # Then popup a selection menu
    SELECTIONS="run\ntodo\nnote\nsystem\nweather"

    # Test calc
    command -v calc >/dev/null 2>&1

    if ! (( $? )); then
        SELECTIONS="$SELECTIONS\ncalc"
    fi

    # Test xdg (apps)
    command -v xdg_menu >/dev/null 2>&1

    if ! (( $? )); then
        SELECTIONS="$SELECTIONS\napps"
    fi

    # Test gtk2theme
    command -v gtk-theme-switch2 >/dev/null 2>&1

    if ! (( $? )); then
        SELECTIONS="$SELECTIONS\ngtk2theme"
    fi

    SELECTION=$(echo -e "$SELECTIONS" | $DMENU -p HYPERMENU)

    if [ -z "$SELECTION" ]; then                   
        exit 0
    fi

    hypermenu $SELECTION
fi
