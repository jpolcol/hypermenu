# Written by James Polanco

#Makefile for HyperMenu
#Install: sudo make install
#Uninstall: sudo make uninstall
prefix     ?= /usr/local
bindir      = ${prefix}/bin
sharedir    = ${prefix}/share
licensesdir = ${sharedir}/licenses
docdir      = ${sharedir}/doc
mandir      = ${sharedir}/man
man1dir     = ${mandir}/man1

install:
	@echo "Installing hypermenu..."
	install -d $(bindir)
	install -Dm755 hypermenu $(destdir)$(bindir)/hypermenu
	install -Dm755 scripts/sleepthen $(destdir)$(bindir)/sleepthen
	install -d $(destdir)$(licensesdir)/hypermenu
	install -Dm644 doc/COPYING $(destdir)$(licensesdir)/hypermenu/COPYING
	install -d $(destdir)$(docdir)/hypermenu
	install -Dm644 doc/README $(destdir)$(docdir)/hypermenu/README
	install -Dm644 doc/TODO $(destdir)$(docdir)/hypermenu/TODO
	install -Dm644 doc/INSTALL $(destdir)$(docdir)/hypermenu/INSTALL
	install -Dm644 doc/AUTHORS $(destdir)$(docdir)/hypermenu/AUTHORS
	install -Dm644 doc/CHANGELOG $(destdir)$(docdir)/hypermenu/CHANGELOG
	install -Dm644 man/hypermenu.1 $(destdir)$(man1dir)/hypermenu.1
	install -d $(destdir)$(sharedir)/hypermenu
	install -Dm644 sxhkdrc.example $(destdir)$(sharedir)/hypermenu/sxhkdrc.example
	install -Dm644 xbindkeysrc.example $(destdir)$(sharedir)/hypermenu/xbindkeysrc.example

uninstall:
	@echo "Uninstalling hypermenu..."
	rm -f $(bindir)/hypermenu
	rm -f $(bindir)/sleepthen
	rm -f $(licensesdir)/hypermenu/COPYING
	rm -R $(licensesdir)/hypermenu
	rm -f $(docdir)/hypermenu/README
	rm -f $(docdir)/hypermenu/TODO
	rm -f $(docdir)/hypermenu/INSTALL
	rm -f $(docdir)/hypermenu/AUTHORS
	rm -f $(docdir)/hypermenu/CHANGELOG
	rm -R $(docdir)/hypermenu
	rm -f $(man1dir)/hypermenu.1
	rm -f $(sharedir)/hypermenu/sxhkdrc.example
	rm -f $(sharedir)/hypermenu/xbindkeysrc.example
	rm -R $(sharedir)/hypermenu"

.PHONY: install uninstall
