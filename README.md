# **HyperMenu** #

https://bitbucket.org/jpolcol/hypermenu

by James Polanco <<jpolcol@gmail.com>>
                                  
### ABOUT ###

**HyperMenu** is a multi-purpose, **dmenu** based utility that features a TODO list 
manager, note manager, calculator, applications menu, system menu, weather menu, and a GTK2 
theme switcher. Designed for speed and efficiency, the program's name was 
inspired by the "hyper" modifier key on most keyboards, often labeled with a 
menu symbol. In fact, it is highly recommended to bind **HyperMenu**’s commands to 
hotkeys with **sxhkd**, **xbindkeys**, or any other X keybinder.

### CONTRIBUTIONS ###

All contributions are welcome, especially ideas on how to manipulate dmenu for useful purposes.

### ADDITIONAL INFORMATION ###

Refer to the hypermenu(1) manual page. Documentation also exists in the doc folder of this distribution.

### LICENSE ###

HyperMenu is distributed under the terms of the GNU General Public License 3.
See the COPYING file that accompanies this distribution.
